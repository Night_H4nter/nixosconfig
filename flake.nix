{
    description = "Main flake for NixOS config";
    
    inputs = { 
        nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.05";
        nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";
        home-manager = {
            url = "github:nix-community/home-manager/release-22.05";
            inputs.nixpkgs.follows = "nixpkgs";
        };
    };

    outputs = { self, nixpkgs, nixpkgs-unstable, home-manager, ... }@inputs: {
        nixosConfigurations = {
            testvm-foreign = nixpkgs.lib.nixosSystem {
                system = "x86_64-linux";
                modules = [ 
                    ./nix.nix
                    ./hosts/testvm-foreign/system.nix
                    ./nixos/conthost.nix
                    ./nixos/nighth4nter.nix
                    ./nixos/distcompat.nix
                    ./nixos/flatpak.nix
                    ./nixos/gameuser.nix
                    ./nixos/guishell-sys.nix
                    ./nixos/minimal.nix
                    ./nixos/networking.nix
                    ./nixos/security.nix
                    ./nixos/sound.nix
                    ./nixos/timedate.nix
                    ./nixos/vmhost.nix
                    ./nixos/security.nix
                    home-manager.nixosModules.home-manager
                    {
                        nixpkgs.config.allowUnfree = true;
                        home-manager = {
                            useGlobalPkgs = true;
                            useUserPackages = true;
                            users.nighth4nter = {
                                home = {
                                    username = "nighth4nter";
                                    homeDirectory = "/home/nighth4nter";
                                };
                                imports = [
                                    ./nixos/guishell-home.nix
                                    ./nix/cli.nix
                                    ./nix/web.nix
                                    ./nix/devops.nix
                                    ./nix/terminals.nix
                                    ./nix/office.nix
                                    ./nix/media.nix
                                ];
                            };
                        };
                    }
                ];
                specialArgs = { inherit inputs; };
            };


        };
        #testvm-foreign = self.nixosConfiguration.testvm-foreign.config.system.build.toplevel;
    };
}
