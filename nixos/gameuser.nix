{ config, pkgs, ... }:

{
    users = {
        users = {
            gameuser = {
                isNormalUser = true;
                initialPassword = "12";
                extraGroups = [ "audio" "networkmanager" ];
                shell = pkgs.zsh;
                home = "/home/gameuser";
                homeMode = "777";
            };
        };
    };
}
