{ config, pkgs, ... }:

{
    services.xserver = {
        enable = true;
        displayManager.sddm.enable = true;      #TODO:use a different display manager
        desktopManager.plasma5.enable = true;   #TODO:get rid of it
        layout = "us,ru,il";
        xkbVariant = "";
        xkbOptions = "grp:win_space_toggle";
        windowManager.xmonad = {
            enable = true;
            enableContribAndExtras = true;
            extraPackages = haskellPackages: [
                haskellPackages.xmonad
            ];
        };
    };
    environment.systemPackages = with pkgs; [
        xorg.xrandr
        xkb-switch
        taffybar
        redshift
    ];
    xdg.portal = {
        enable = true;
        extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
    };
}
