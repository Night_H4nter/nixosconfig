{ config, ... }:

{
    time.timeZone = "Europe/Saratov";
    i18n = {
        defaultLocale = "en_US.utf8";
        extraLocaleSettings = {
            LC_ADDRESS = "he_IL.utf8";
            LC_IDENTIFICATION = "he_IL.utf8";
            LC_MEASUREMENT = "he_IL.utf8";
            LC_MONETARY = "he_IL.utf8";
            LC_NAME = "he_IL.utf8";
            LC_NUMERIC = "he_IL.utf8";
            LC_PAPER = "he_IL.utf8";
            LC_TELEPHONE = "he_IL.utf8";
            LC_TIME = "he_IL.utf8";
        };
    };
}
