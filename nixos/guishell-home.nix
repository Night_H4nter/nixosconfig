{ config, pkgs, lib, inputs, ... }:

let
    guishelldotfiles = pkgs.fetchFromGitLab {
        owner = "Night_H4nter";
        repo = "guishellconfig";
        rev = "d2f2a0cf403b195b825b6725658a31ca0a235e42";
        sha256 = "eQgkknOwMbcj9i1nJv1ZznGTTqHTXiQNYyY9WIhtAxM=";
    };
in
{
    home = {
        file = {
            xmonad = {
                source = "${guishelldotfiles}/xmonad/.config/xmonad/xmonad.hs";
                target = ".config/xmonad/xmonad.hs";
            };
            picom = { 
                source = "${guishelldotfiles}/picom/.config/picom/picom.conf";
                target = ".config/picom/picom.conf";
            };
            taffybar = {
                source = "${guishelldotfiles}/taffybar/.config/taffybar/taffybar.hs";
                target = ".config/taffybar/taffybar.hs";
            };
            redshift = { 
                source = "${guishelldotfiles}/redshift/.config/redshift/redshift.conf";
                target = ".config/redshift/redshift.conf";
            };
        };
        sessionVariables = {
            CM_LAUNCHER = "rofi";
            CM_HISTLENGTH = "10";
            #TODO:add other clipmenu settings if needed
        };
        packages = with pkgs; [
            dmenu       #TODO:do i need this?
            rofi
        ];
    };
    services = {
        clipmenu.enable = true;
        dunst.enable = true;
        #redshift = {
        #    enable = true;
        #    tray = true;
        #};
        #picom.enable = true;

        #TODO:add taffybar
    };
}
