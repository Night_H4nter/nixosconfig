{ config, pkgs, ... }:

{
    services = { 
        flatpak.enable = true;
        #TODO:for flatpak to work, the flathub remote should be added.
        #nixos doesn't have it so by default, nor does it support doing it
        #declaratively
        #NODECL: flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

        #TODO:this needs to be commented out for plasma to work,
        #need to figure out if needs to be disabled implicitly or not
        #upower.enable = false;
    };
}
