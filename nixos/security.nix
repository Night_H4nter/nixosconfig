{ config, pkgs, ... }:

{
    security.rtkit.enable = true;
    environment.systemPackages = with pkgs; [
        vulnix
        lynis
        clamav
        firejail
    ];
    users = {
        mutableUsers = false;
        users.root.hashedPassword = "!";
    };
}
