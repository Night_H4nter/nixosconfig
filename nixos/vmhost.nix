{ config, pkgs, ... }:

{
    virtualisation = {
        libvirtd = {
            enable = true;
        };
        #TODO:marked as broken, wait till it's fixed
        #virtualbox = { 
        #    host = {
        #        enable = true;
        #        enableExtensionPack = true;
        #    };
        #};
    };
}
