{ config, pkgs, ... }:

{
    users = {
        users = {
            nighth4nter = {
                isNormalUser = true;
                description = "Daniel";
                extraGroups = [
                    "networkmanager"
                    "wheel"
                    "video"
                    "audio"
                    "storage"
                    "kvm"
                    "libvirtd"
                ];
                initialPassword = "12";
                shell = pkgs.zsh;
                home = "/home/nighth4nter";
                homeMode = "700";
                subUidRanges = [{ startUid = 100000; count = 65536; }];
                subGidRanges = [{ startGid = 100000; count = 65536; }];
            };
        };
        extraGroups.vboxusers.members = [ "nighth4nter" ];
    };
}
