{ config, pkgs, ... }:

{
    environment.systemPackages = with pkgs; [
        bash
        git
        neovim
        htop
        file
    ];    
    programs.zsh.enable = true;
}
