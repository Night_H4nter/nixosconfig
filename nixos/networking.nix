{ config, pkgs, ... }:

{
    networking = { 
        networkmanager.enable = true;
        firewall = { 
            allowedTCPPorts = [  ];
            allowedUDPPorts = [  ];
        };
    };
    environment.systemPackages = with pkgs; [
        wireguard-tools
        shadowsocks-rust
        openssh

        wget
        curl
    ];
}
