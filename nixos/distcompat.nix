{ config, pkgs, ... }:

{
    environment.systemPackages = with pkgs; [
        distrobox
        xorg.xhost
    ];
    #in order for gui apps inside distrobox containers to work,
    #this should be run on every login
    #NODECL: xhost +si:localuser:$USER
}
