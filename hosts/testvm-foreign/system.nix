{ config, pkgs, ... }:

{
    imports =
        [ 
        ./hardware.nix
        ];
    boot = {
        loader.grub = {
            enable = true;
            useOSProber = false;
            device = "/dev/vda";
        };
        #kernelPackages = pkgs.linuxPackages_latest;
    };

    networking.hostName = "testvm-foreign";

    system.stateVersion = "22.05";
}
