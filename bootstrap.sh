#!/usr/bin/env bash

set -e

echo "Installing on $TARGETDRIVE."
parted -s "$TARGETDRIVE" mklable msdos
# parted -s -a optimal /dev/sdX mkpart "primary" "ext4" "0%" "100%"
mkfs.ext4 -L nixroot "$TARGETDRIVE"
parted -s nixroot set 1 boot on
# parted -s /dev/sdX set 1 lvm on
mount /dev/disk/by-label/nixroot /mnt
mkdir -p /mnt/etc/nixos
nixos-generate-config --root /mnt
rm "/mnt/etc/nixos/configuration.nix"
cp "nixosconfig/flake/nixos/nhmd/configuration.nix" "/mnt/etc/nixos/configuration.nix"

nixos-install --no-root-passwd 
