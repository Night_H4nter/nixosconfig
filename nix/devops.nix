{ config, pkgs, ... }:

{
    home = {
        packages = with pkgs; [
            ansible
            salt
            stow
        ];    
    };
}
