{ config, pkgs, ... }:

let
    appdotfiles = pkgs.fetchFromGitLab {
        owner = "Night_H4nter";
        repo = "appconfig";
        rev = "11d50ed05a2e06f221a70722f2089e1de2126c35";
        sha256 = "sT1pV6YgzXF9ypRJkK6MPJrKyv6j+o1TY7QXDjwl2XU=";
    };
in
{
    home = {
        packages = with pkgs; [
            zathura
            libreoffice-qt
            hunspellDicts.he_IL
            hunspellDicts.ru_RU
            pandoc
        ];
        file = {
            zathura = { 
                source = "${appdotfiles}/zathura/.config/zathura/zathurarc";
                target = ".config/zathura/zathurarc";
            };
        };
    };
}
