{ config, pkgs, ... }:

let
    appdotfiles = pkgs.fetchFromGitLab {
        owner = "Night_H4nter";
        repo = "appconfig";
        rev = "11d50ed05a2e06f221a70722f2089e1de2126c35";
        sha256 = "sT1pV6YgzXF9ypRJkK6MPJrKyv6j+o1TY7QXDjwl2XU=";
    };
in
{
    home = {
        packages = with pkgs; [
            vlc
            mpv
            youtube-dl
            ffmpeg
            nsxiv
            obs-studio
        ];
        file = {
            flameshot = {
                source = "${appdotfiles}/flameshot/.config/flameshot/flameshot.ini";
                target = ".config/flameshot/flameshot.ini";
            };
        };
    };
    services = {
        flameshot.enable = true;
    };
}
