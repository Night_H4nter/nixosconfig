{ config, pkgs, lib, inputs, ... }:

let
    clishelldotfiles = pkgs.fetchFromGitLab {
        owner = "Night_H4nter";
        repo = "termconfig";
        rev = "e27d137598e40782aeed7552eeeb80dda97ef331";
        sha256 = "f0R28iuYTCrjh2xhFzAkZqtxfjKm0QVDC74AOSs6n2Q=";
    };
in
{
    home = {
        file = {
            zprofile = {
                source = "${clishelldotfiles}/zsh/.zprofile";
                target = ".zprofile";
            };
            zshrc = {
                source = "${clishelldotfiles}/zsh/.zshrc";
                target = ".zshrc";
            };
            lazygit = { 
                source = "${clishelldotfiles}/lazygit/.config/lazygit/config.yml";
                target = ".config/lazygit/config.yml";
            };
            distrobox = { 
                source = "${clishelldotfiles}/distrobox/.config/distrobox/distrobox.conf";
                target = ".config/distrobox/distrobox.conf";
            };
            podman = { 
                source = "${clishelldotfiles}/podman/.config/podman";
                target = ".config/podman";
                recursive = true;
            };
            tmux = { 
                source = "${clishelldotfiles}/tmux/.config/tmux";
                target = ".config/tmux";
                recursive = true;
            };
            tym = { 
                source = "${clishelldotfiles}/tym/.config/tym";
                target = ".config/tym";
                recursive = true;
            };
            vifm = { 
                source = "${clishelldotfiles}/vifm/.config/vifm";
                target = ".config/vifm";
                recursive = true;
            };
        };    
        packages = with pkgs; [
            bash
            tree
            jq
            file
            
            git
            xclip
            
            gnumake
            automake
            gcc
            
            tmux
            lazygit
            neovim
            vifm
            htop
        ];    
    };
    #programs.zsh.enable = true;
}
