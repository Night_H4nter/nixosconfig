{ config, pkgs, lib, inputs, ... }:

let
    appdotfiles = pkgs.fetchFromGitLab {
        owner = "Night_H4nter";
        repo = "appconfig";
        rev = "11d50ed05a2e06f221a70722f2089e1de2126c35";
        sha256 = "sT1pV6YgzXF9ypRJkK6MPJrKyv6j+o1TY7QXDjwl2XU=";
    };
in
{
    home = {
        file = {
            kitty = { 
                source = "${appdotfiles}/kitty/.config/kitty";
                target = ".config/kitty";
                recursive = true;
            };
            termonad = { 
                source = "${appdotfiles}/termonad/.config/termonad";
                target = ".config/termonad";
                recursive = true;
            };
        };
        packages = with pkgs; [
            alacritty
            kitty
            termonad
        ];
    };
}
