# My NixOS config

### Directory structure

(or what i want directory structure to look like eventually)

### system, users, home

Modules containing basic building blocks that configure NixOS itself, 
different custom users (be that human users or some service ones that I
personally need), and home-manager, respectively.

### modules

Modules bundled together for ease of use.

### templates

Templates for different kinds of machines built from modules.

### hosts

Host configurations built from templates with host-specific overrides
and (maybe) additions.
